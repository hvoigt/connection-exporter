package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	networkInterface string
	speedfile        string
	measuringSpeed   bool
	registeredGauges map[prometheus.Gauge]bool
)

func recordMetrics() {
	go func() {
		for {
			opsProcessed.Inc()
			readVodafoneSpeed()
			time.Sleep(2 * time.Second)
		}
	}()
}

func getBandwidth(values map[string]interface{}, name string) (float64, bool) {
	value, ok := values[name].(map[string]interface{})
	if !ok {
		return 0.0, false
	}
	raw_bandwidth, ok := value["bandwidth"].(float64)
	if !ok {
		return 0.0, false
	}
	return raw_bandwidth * 10, true
}

func writeResult(output []byte) {
	file, err := os.Create(speedfile)
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	if _, err := file.Write(output); err != nil {
		log.Println(err)
		return
	}
}

func readPipe(channel chan []byte, done chan error, pipe io.ReadCloser) {
	defer close(channel)
	defer pipe.Close()

	buf := new(bytes.Buffer)
	_, err := buf.ReadFrom(pipe)
	if err != nil {
		log.Printf("Error reading: %v\n", err)
		done <- err
		return
	}
	channel <- buf.Bytes()
}

func measureSpeed() {
	if measuringSpeed {
		return
	}
	measuringSpeed = true
	log.Println("Measuring speed ...")
	defer func() {
		measuringSpeed = false
		log.Println("Done measuring ...")
	}()

	cmd := exec.Command("speedtest",
		"-I", networkInterface,
		"-f", "json")

	cmd.Env = append(os.Environ(),
		"HOME=/root")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Print(err)
		return
	}
	stderr, err := cmd.StderrPipe()
	if err != nil {
		log.Print(err)
		return
	}

	stdoutChan := make(chan []byte)
	done := make(chan error, 1)
	go readPipe(stdoutChan, done, stdout)

	stderrChan := make(chan []byte)
	go readPipe(stderrChan, done, stderr)

	if err := cmd.Start(); err != nil {
		log.Print(err)
		return
	}

	select {
	case <-time.After(60 * time.Second):
		if err := cmd.Process.Kill(); err != nil {
			log.Print("failed to kill process: ", err)
			return
		}
		log.Println("speedtest hang: killed")
	case err := <-done:
		if err != nil {
			log.Printf("process finished with error = %v", err)
			return
		}
	case output := <-stdoutChan:
		writeResult(output)
	case output := <-stderrChan:
		writeResult(output)
	}
	if err := cmd.Wait(); err != nil {
		log.Printf("speedtest failed: %v\n", err)
	}
}

func setGauge(gauge prometheus.Gauge, value float64) {
	if _, registered := registeredGauges[gauge]; !registered {
		prometheus.MustRegister(gauge)
		registeredGauges[gauge] = true
	}

	gauge.Set(value)
}

func invalidateGauge(gauge prometheus.Gauge) {
	prometheus.Unregister(gauge)
	delete(registeredGauges, gauge)
}

func readVodafoneSpeed() {
	info, err := os.Stat(speedfile)
	if err != nil {
		invalidateGauge(vodafone_speedtest_download)
		invalidateGauge(vodafone_speedtest_upload)
		go measureSpeed()
		return
	}

	modTime := info.ModTime()
	if time.Now().Sub(modTime).Minutes() >= 5 {
		go measureSpeed()
	}

	if time.Now().Sub(modTime).Minutes() >= 10 {
		invalidateGauge(vodafone_speedtest_download)
		invalidateGauge(vodafone_speedtest_upload)
	}

	file, err := os.Open(speedfile)
	if err != nil {
		invalidateGauge(vodafone_speedtest_download)
		invalidateGauge(vodafone_speedtest_upload)
		go measureSpeed()
		return
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		invalidateGauge(vodafone_speedtest_download)
		invalidateGauge(vodafone_speedtest_upload)
		return
	}

	var values map[string]interface{}
	json.Unmarshal([]byte(data), &values)

	download, ok := getBandwidth(values, "download")
	if ok {
		setGauge(vodafone_speedtest_download, download)
	} else {
		invalidateGauge(vodafone_speedtest_download)
		go measureSpeed()
	}

	upload, ok := getBandwidth(values, "upload")
	if ok {
		setGauge(vodafone_speedtest_upload, upload)
	} else {
		invalidateGauge(vodafone_speedtest_upload)
		go measureSpeed()
	}
}

var (
	opsProcessed = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "myapp_processed_ops_total",
		Help: "The total number of processed events",
	})
	vodafone_speedtest_download = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "vodafone_speedtest_download",
		Help: "Current vodafone download speed",
	})
	vodafone_speedtest_upload = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "vodafone_speedtest_upload",
		Help: "Current vodafone upload speed",
	})
)

func init() {
	prometheus.MustRegister(opsProcessed)
	registeredGauges = make(map[prometheus.Gauge]bool, 0)
}

func main() {
	measuringSpeed = false
	networkInterface = "enp1s0"
	speedfile = "/tmp/vodafone_speedtest_current"

	recordMetrics()

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe("192.168.151.1:2112", nil)
}
