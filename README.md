
# ookla speedtest-cli result exporter

This is a prometheus exporter for [ookla speedtest-cli](https://www.speedtest.net/de/apps/cli) results. It reports latest saved results and schedules new runs when enough time has passed (currently 5 minutes). Other speedtest exporters are limited when it comes to high speed internet connections like Vodafone Gigabit Cable. With this exporter we use the official speedtest-cli which does not suffer from these problems.

This work extends the [super easy internet-monitoring stack](https://github.com/geerlingguy/internet-monitoring) which itself is based on [grafana/prometheus setup using docker-compose](https://github.com/vegasbrianc/github-monitoring).

## Dislaimer

*This was/is a quick hack I use to monitor whether my connection lives up to its specifications, so there are still lots of opportunities to improve the flexibility of the code :)*

## Configuration

There is currently no configuration files. All customizations are hardcoded in the `main.go` file.

## Prerequisites

* golang
* speedtest-cli

## Installation

```
go build ./...
sudo mv connection-exporter /usr/local/bin/
sudo cp connection-exporter.service /etc/systemd/system/
systemctl enable connection-exporter.service
systemctl start connection-exporter.service
```

Now you can configure prometheus to scrape from `<host-ip>:2112` and it should report metrics named `vodafone_speedtest_download` and `vodafone_speedtest_upload` as bits/s.
